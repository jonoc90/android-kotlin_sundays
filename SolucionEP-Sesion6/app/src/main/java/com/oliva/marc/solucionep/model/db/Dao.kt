package com.oliva.marc.solucionep.model.db

import androidx.room.*
import androidx.room.Dao

@Dao
interface Dao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(item: T)

    @Delete
    fun delete(item:T)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(item: T)
}
package com.oliva.marc.solucionep.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.oliva.marc.solucionep.R
import com.oliva.marc.solucionep.Utils
import com.oliva.marc.solucionep.adapter.ProductRecyclerViewAdapter
import com.oliva.marc.solucionep.model.Product
import kotlinx.android.synthetic.main.fragment_accesibles.*

class AccesiblesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_accesibles, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val listAccesibles = Utils().getAccesiblesList()
        setListProductsInAdapter(listAccesibles)
        filter_mayor_menor_button.setOnClickListener {
            setListProductsInAdapter(Utils().mayorToMenor(listAccesibles).toMutableList() as ArrayList<Product>)
        }
        filter_menor_mayor_button.setOnClickListener {
            setListProductsInAdapter(Utils().menorToMayor(listAccesibles).toMutableList() as ArrayList<Product>)
        }
        filter_stock_button.setOnClickListener {
            setListProductsInAdapter(Utils().withStock(listAccesibles).toMutableList() as ArrayList<Product>)
        }

    }
    fun setListProductsInAdapter(listProducts:ArrayList<Product>){
        accesibles_recyclerview.adapter = ProductRecyclerViewAdapter(context,listProducts )
    }


}

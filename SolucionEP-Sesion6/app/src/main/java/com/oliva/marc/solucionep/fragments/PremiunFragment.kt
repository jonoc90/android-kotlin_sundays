package com.oliva.marc.solucionep.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.oliva.marc.solucionep.R
import com.oliva.marc.solucionep.Utils
import com.oliva.marc.solucionep.adapter.ClickListener
import com.oliva.marc.solucionep.adapter.ProductRecyclerViewAdapter
import com.oliva.marc.solucionep.adapter.RecyclerTouchListener
import com.oliva.marc.solucionep.model.Product
import kotlinx.android.synthetic.main.fragment_premiun.*

class PremiunFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_premiun, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val listPremiun = Utils().getPremiunList()
        setListProductsInAdapter(listPremiun)
        filter_mayor_menor_button.setOnClickListener {
            setListProductsInAdapter(Utils().mayorToMenor(listPremiun).toMutableList() as ArrayList<Product>)
        }
        filter_menor_mayor_button.setOnClickListener {
            setListProductsInAdapter(Utils().menorToMayor(listPremiun).toMutableList() as ArrayList<Product>)
        }
        filter_stock_button.setOnClickListener {
            setListProductsInAdapter(Utils().withStock(listPremiun).toMutableList() as ArrayList<Product>)
        }
    }
    fun setListProductsInAdapter(listProducts:ArrayList<Product>){

        premiun_recyclerview.adapter = ProductRecyclerViewAdapter(context,listProducts )
        premiun_recyclerview.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                premiun_recyclerview,
                object : ClickListener {
                    override fun onClick(view: View?, position: Int) {
                        val product: Product = listProducts[position]
                        Toast.makeText(
                            context,
                            product.detail + " is selected!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onLongClick(view: View?, position: Int) {}
                })
        )
    }
}

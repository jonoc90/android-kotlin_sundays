package com.oliva.marc.solucionep

import com.oliva.marc.solucionep.model.Product

class Utils {
    fun getFirstProductsList(): ArrayList<Product> {
        val listProduct = ArrayList<Product>()
        //mas vendidos
        listProduct.add(
            Product(
                null, "arroz", "arroz blanco", null, 25.0, 3, false,
                false, false, true
            )
        )
        listProduct.add(
            Product(
                null, "arroz", "arroz negro", null, 28.0, 0, false,
                false, false, true
            )
        )
        listProduct.add(
            Product(
                null, "arroz", "arroz morado", null, 32.0, 3, false,
                false, false, true
            )
        )
        listProduct.add(
            Product(
                null, "arroz", "arroz mostaza", null, 15.0, 0, false,
                false, false, true
            )
        )
        listProduct.add(
            Product(
                null, "arroz", "arroz mostaza", null, 15.0, 3, false,
                false, false, true
            )
        )
        //premiun
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 100.0, 0, false,
                false, true, false
            )
        )
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 50.0, 8, false,
                false, true, false
            )
        )
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 30.0, 8, false,
                false, true, false
            )
        )
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 80.0, 8, false,
                false, true, false
            )
        )
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 30.0, 8, false,
                false, true, false
            )
        )
        listProduct.add(
            Product(
                null, "harina", "harina sin preparar", null, 10.0, 0, false,
                false, true, false
            )
        )
        //accesibles
        listProduct.add(
            Product(
                null, "lentejas", "lentejas verdes", null, 8.0, 2, false,
                true, false, false
            )
        )
        listProduct.add(
            Product(
                null, "lentejas", "lentejas verdes", null, 8.0, 0, false,
                true, false, false
            )
        )
        listProduct.add(
            Product(
                null, "lentejas", "lentejas verdes", null, 8.0, 2, false,
                true, false, false
            )
        )
        return listProduct
    }

    fun getMasVendidosList(): ArrayList<Product> {
        return getFirstProductsList().filter {
            it.isBestSelling
        } as ArrayList<Product>
    }

    fun getAccesiblesList(): ArrayList<Product> {
        return getFirstProductsList().filter {
            it.isCheap
        } as ArrayList<Product>
    }

    fun getPremiunList():ArrayList<Product>{
        return getFirstProductsList().filter {
            it.isPremiun
        } as ArrayList<Product>
    }

    fun mayorToMenor(products: ArrayList<Product>): List<Product> {
        return products.sortedWith(compareByDescending { it.price })
    }

    fun menorToMayor(products: ArrayList<Product>): List<Product> {
        return products.sortedWith(compareBy { it.price })
    }

    fun withStock(products: ArrayList<Product>): List<Product> {
        return products.filter { it.stock > 0 }
    }
}
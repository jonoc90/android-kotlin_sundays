package com.oliva.marc.solucionep

import android.app.Application
import com.oliva.marc.solucionep.model.db.DBRoomRepository

class InventoryApplication : Application(){
    private lateinit var repository: DBRoomRepository
    override fun onCreate() {
        super.onCreate()
        repository = DBRoomRepository(this)
    }
    fun getInventoryRepository():DBRoomRepository{
        return repository
    }
}

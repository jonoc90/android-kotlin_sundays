package com.oliva.marc.solucionep.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.oliva.marc.solucionep.DetailProductActivity
import com.oliva.marc.solucionep.R
import com.oliva.marc.solucionep.model.Product
import com.oliva.marc.solucionep.model.db.DBRoomRepository

class ProductRecyclerViewAdapter(val context: Context?, val products: ArrayList<Product>) :
    RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder>() {

    private var dbRepository: DBRoomRepository? = null
    //var products: ArrayList<Product>? = null

    init {
        dbRepository = DBRoomRepository(context)
        //products = lisProducts

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var nameProduct = itemView.findViewById<TextView>(R.id.name_product_textview)
        var detailProduct = itemView.findViewById<TextView>(R.id.detail_product_textview)
        var stockProduct = itemView.findViewById<TextView>(R.id.stock_product_textview)
        var priceProduct = itemView.findViewById<TextView>(R.id.price_product_textview)
        var favoriteButton = itemView.findViewById<ImageView>(R.id.heart_imageview)

        init {
            super.itemView
        }

        fun setDataProduct(product: Product?, dbRepository: DBRoomRepository?) {

            nameProduct.text = product?.name
            detailProduct.text = product?.detail
            stockProduct.text = itemView.context.getString(R.string.stock_description,product?.stock.toString())
            priceProduct.text = itemView.context.getString(R.string.price_description,product?.price.toString())
            if (product != null) {
                if (product.isFavorite) {
                    favoriteButton.setImageResource(R.drawable.ic_heart_filled)
                } else {
                    favoriteButton.setImageResource(R.drawable.ic_heart_empty)
                }
            }
            favoriteButton.setOnClickListener {
                if (product != null) {
                    changeStatusFavoriteProduct(product, dbRepository)
                }
            }
        }

        private fun changeStatusFavoriteProduct(product: Product, dbRepository: DBRoomRepository?) {
            if (!product.isFavorite) {
                product.isFavorite = true
                dbRepository?.addProduct(
                    product
                )
                favoriteButton.setImageResource(R.drawable.ic_heart_filled)
            } else {
                dbRepository?.deleteProduct(
                    product
                )
                favoriteButton.setImageResource(R.drawable.ic_heart_empty)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_product,
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setDataProduct(products.get(position), dbRepository)
       /* holder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("product", products?.get(position))
            context?.startActivity(
                Intent(
                    context,
                    DetailProductActivity::class.java
                ).putExtra("bundle", bundle)
            )
        }*/
    }

}
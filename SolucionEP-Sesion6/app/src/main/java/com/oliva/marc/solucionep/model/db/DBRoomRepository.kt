package com.oliva.marc.solucionep.model.db

import android.content.Context
import android.os.AsyncTask
import com.oliva.marc.solucionep.model.Product
import java.lang.Exception

class DBRoomRepository(context: Context?) {
    private var productDao : ProductDao?

    init {
        val db = AppDataBase.getInstance(context)
        productDao = db?.productDao()

    }
    fun getAllProducts(populateCallBack: PopulateCallBack){
        productDao.let {
            PopulateAsyncTask(it,populateCallBack).execute()
        }

    }
    fun addProduct(product: Product){
        productDao.let {
            InsertAsyncTask(productDao).execute(product)
        }
    }
    fun updateProduct(product: Product){
        productDao.let {
            UpdateAsyncTask(productDao).execute(product)
        }
    }
    fun deleteProduct(product: Product){
        productDao.let {
            DeleteAsyncTask(productDao).execute(product)
        }
    }

    interface PopulateCallBack{
        fun onSuccess(products:List<Product>?)
        fun onFailure(e:Exception)
    }
    private class PopulateAsyncTask internal constructor(
        private val mAsyncTaskDao: ProductDao?,
        private val populateCallBack: PopulateCallBack
    ):AsyncTask<Void,Void,List<Product>>(){
        override fun doInBackground(vararg params: Void?): List<Product>? {
            return mAsyncTaskDao?.getAll()
        }

        override fun onPostExecute(result: List<Product>?) {
            super.onPostExecute(result)
            populateCallBack.onSuccess(result)

        }
    }

    private class InsertAsyncTask internal constructor(
        private val mAsyncTaskDao: ProductDao?
    ):AsyncTask<Product,Void,Boolean>(){
        override fun doInBackground(vararg params: Product): Boolean? {
            mAsyncTaskDao?.add(params[0])
            return true
        }
    }

    private class UpdateAsyncTask internal constructor(
        private val mAsyncTaskDao:ProductDao?
    ):AsyncTask<Product,Void,Void>(){
        override fun doInBackground(vararg params: Product): Void? {
            mAsyncTaskDao?.update(params[0])
            return null
        }

    }
    private class DeleteAsyncTask internal constructor(
        private val mAsyncTaskDao: ProductDao?
    ):AsyncTask<Product,Void,Void>(){
        override fun doInBackground(vararg params: Product): Void? {
            mAsyncTaskDao?.delete(params[0])
            return null
        }

    }

}
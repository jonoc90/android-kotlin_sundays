package com.oliva.marc.solucionep.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.oliva.marc.solucionep.model.Product

@Database(entities = [Product::class],version = 1)
abstract class AppDataBase:RoomDatabase() {
    abstract fun productDao():ProductDao

    companion object{
        private var INSTANCE : AppDataBase?=null
        private const val DBName = "inventorydb"

        fun getInstance(context: Context?): AppDataBase? {
            if (INSTANCE == null){
                synchronized(AppDataBase::class){
                    INSTANCE = Room.databaseBuilder(
                        context!!.applicationContext,
                        AppDataBase::class.java, DBName)
                        .build()
                }
            }
            return INSTANCE
        }
    }
    fun destroyInstance(){
        INSTANCE = null
    }
}
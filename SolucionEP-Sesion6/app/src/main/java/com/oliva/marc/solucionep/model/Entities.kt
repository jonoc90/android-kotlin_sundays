package com.oliva.marc.solucionep.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "product")
data class Product(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    @ColumnInfo(name = "name")
    var name: String,
    @ColumnInfo(name = "detail")
    var detail: String,
    @ColumnInfo(name = "image")
    var image: String?,
    @ColumnInfo(name = "price")
    var price: Double,
    @ColumnInfo(name = "stock")
    var stock: Int,
    @ColumnInfo(name = "isFavorite")
    var isFavorite: Boolean,
    @ColumnInfo(name = "isCheap")
    var isCheap: Boolean,
    @ColumnInfo(name = "isPremiun")
    var isPremiun: Boolean,
    @ColumnInfo(name = "isBestSelling")
    var isBestSelling: Boolean
):Serializable

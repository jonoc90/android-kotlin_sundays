package com.oliva.marc.solucionep.model.db

import androidx.room.Query
import com.oliva.marc.solucionep.model.Product

@androidx.room.Dao
interface ProductDao :Dao<Product>{
    @Query("select * from product")
    fun getAll():List<Product>
}
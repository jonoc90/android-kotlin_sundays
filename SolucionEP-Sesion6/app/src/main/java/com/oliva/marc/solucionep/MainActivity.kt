package com.oliva.marc.solucionep

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        ui()

    }
    private fun ui(){
        signin_button.setOnClickListener {
            startActivity(Intent(this,DashboardActivity::class.java))
        }
    }

}

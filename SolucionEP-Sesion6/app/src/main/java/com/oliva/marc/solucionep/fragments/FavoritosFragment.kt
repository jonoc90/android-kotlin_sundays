package com.oliva.marc.solucionep.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oliva.marc.solucionep.DetailProductActivity

import com.oliva.marc.solucionep.R
import com.oliva.marc.solucionep.adapter.ClickListener
import com.oliva.marc.solucionep.adapter.ProductRecyclerViewAdapter
import com.oliva.marc.solucionep.adapter.RecyclerTouchListener
import com.oliva.marc.solucionep.model.Product
import com.oliva.marc.solucionep.model.db.DBRoomRepository
import kotlinx.android.synthetic.main.fragment_favoritos.*
import java.lang.Exception

class FavoritosFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favoritos, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DBRoomRepository(context).getAllProducts(object: DBRoomRepository.PopulateCallBack {
            override fun onSuccess(products: List<Product>?) {
                setListProductsInAdapter(products as ArrayList<Product>)
            }

            override fun onFailure(e: Exception) {

            }

        })

    }

    fun setListProductsInAdapter(listProducts:ArrayList<Product>){
        favoritos_recyclerview.adapter = ProductRecyclerViewAdapter(context, listProducts)
        /*favoritos_recyclerview.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                favoritos_recyclerview,
                object : ClickListener {
                    override fun onClick(view: View?, position: Int) {
                        val product: Product = listProducts[position]
                        val bundle = Bundle()
                        bundle.putSerializable("product",product)
                        startActivity(Intent(context, DetailProductActivity::class.java).putExtra("bundle",bundle))
                    }
                    override fun onLongClick(view: View?, position: Int) {}
                })
        )*/
    }
}

package com.oliva.marc.solucionep

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.oliva.marc.solucionep.model.Product
import com.oliva.marc.solucionep.model.db.DBRoomRepository
import kotlinx.android.synthetic.main.activity_detail_product.*

class DetailProductActivity : AppCompatActivity() {
    private var repository: DBRoomRepository? = null
    private var productId : Int ? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Detalle Producto"
        setUpData()
        ui()
        setUpRepository()

    }

    fun setUpRepository() {
        repository = (application as InventoryApplication).getInventoryRepository()
    }

    fun ui() {
        update_product_button.setOnClickListener {
            repository?.updateProduct(getProductUpdated())
            finish()
        }
    }

    fun getProductUpdated(): Product {
        return Product(
            productId,
            name_product_edittext.text.toString(),
            detail_product_edittext.text.toString(), null,
            price_product_edittext.text.toString().toDouble(),
            stock_product_edittext.text.toString().toInt(),
            true, accesible_rb.isChecked,
            ispremiun_rb.isChecked, accesible_rb.isChecked
        )
    }

    fun setUpData() {
        intent.extras.let {
            val bundle: Bundle = it?.get("bundle") as Bundle
            val product: Product = bundle.getSerializable("product") as Product
            productId = product.id
            name_product_edittext.setText(product.name)
            detail_product_edittext.setText(product.detail)
            price_product_edittext.setText(product.price.toString())
            stock_product_edittext.setText(product.stock.toString())

            ispremiun_rb.isChecked = product.isPremiun
            masvendido_rb.isChecked = product.isBestSelling
            accesible_rb.isChecked = product.isCheap

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

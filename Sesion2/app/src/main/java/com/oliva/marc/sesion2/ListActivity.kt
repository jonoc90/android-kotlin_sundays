package com.oliva.marc.sesion2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        //Example ListView
        val listFruits = mutableListOf("platano","uva","naranja","melon","papaya")
        val adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,listFruits)
        fruits_listview.adapter = adapter

        fruits_listview.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                val fruit = parent.getItemAtPosition(position).toString()
                Toast.makeText(this,fruit,Toast.LENGTH_SHORT).show()
            }
        //Example Spinner
        val listPets = mutableListOf("perro","gato","loro")
        val adapter2  = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,listPets)
        pets_spinner.adapter = adapter2

        pets_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }

        }

        //Example GridView
        val listClothes = mutableListOf("camisa","polo","pantalon","corbata","casaca","chompa")
        val adapter3 =  ArrayAdapter(this,android.R.layout.simple_list_item_1,listClothes)
        clothes_gridview.adapter = adapter3

    }

    override fun onDestroy() {
        Log.i(ListActivity::class.java.name,"ON DESTROY")

        super.onDestroy()
    }
}

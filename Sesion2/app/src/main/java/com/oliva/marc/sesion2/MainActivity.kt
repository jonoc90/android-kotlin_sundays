package com.oliva.marc.sesion2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(MainActivity::class.java.name,"ON CREATE")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        signin_button.setOnClickListener {
            val username = username_edittext.text
            val password =password_edittext.text
            if (!username.isNullOrEmpty() && !password.isNullOrEmpty()){
                startActivity(Intent(this,ListActivity::class.java))
            }else{
                Toast.makeText(this,"Llena los campos vacios",Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(this).inflate(R.menu.menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.opcion_one -> Toast.makeText(this,"OPcion 1",Toast.LENGTH_SHORT).show()
            R.id.opcion_two -> Toast.makeText(this,"OPcion 2",Toast.LENGTH_SHORT).show()
            R.id.opcion_three -> Toast.makeText(this,"OPcion 3",Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        Log.i(MainActivity::class.java.name,"ON START")
        super.onStart()
    }

    override fun onResume() {
        Log.i(MainActivity::class.java.name,"ON RESUME")
        super.onResume()
    }

    override fun onPause() {
        Log.i(MainActivity::class.java.name,"ON PAUSE")

        super.onPause()
    }

    override fun onStop() {
        Log.i(MainActivity::class.java.name,"ON STOP")

        super.onStop()
    }

    override fun onDestroy() {
        Log.i(MainActivity::class.java.name,"ON DESTROY")

        super.onDestroy()
    }
}

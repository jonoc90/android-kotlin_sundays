package com.oliva.marc.sesion62

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ui()
    }

    private fun ui() {
        default_maps_button.setOnClickListener {
            goToDefaulMapsActivity()
        }
        places_maps_button.setOnClickListener {
            goToPlacesMapsActivity()
        }
        types_maps_button.setOnClickListener {
            goToTypesMapsActivity()
        }
    }
    fun goToDefaulMapsActivity(){
        startActivity(Intent(this,MapsUbicationActivity::class.java))
    }
    fun goToPlacesMapsActivity(){
        startActivity(Intent(this,MapsActivity::class.java))
    }
    fun goToTypesMapsActivity(){
        startActivity(Intent(this,MapsTypesActivity::class.java))
    }
}

package com.oliva.marc.sesion62

object Places {
    object academiaMoviles  {
        val latitude = -12.0889076
        val longitude = -77.0343839
    }
    object compuPalace{
        val latitude = -12.1166086
        val longitude = -77.0289155
    }
    object realPlaza{
        val latitude = -12.056575
        val longitude = -77.0375943
    }
}
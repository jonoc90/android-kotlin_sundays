package com.oliva.marc.sesion62

import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.lang.Exception
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback
//, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener,GoogleMap.OnInfoWindowClickListener
{

    private lateinit var mMap: GoogleMap
    private lateinit var mGeocoder: Geocoder
    private lateinit var markerPrueba: Marker
    private lateinit var markerDrag: Marker
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mGeocoder = Geocoder(this)
        mMap = googleMap

        // Add a marker in Academia Moviles and move the camera
        //marker por default
        val am = LatLng(Places.academiaMoviles.latitude, Places.academiaMoviles.longitude)
        mMap.addMarker(
            MarkerOptions().position(am).title("Academia Moviles")
                .snippet("Academia Moviles en Peru")
            //arrastrar y soltar marker
            //.draggable(true)
            //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        )

        //marker desde 0
/*        val prueba = LatLng(Places.compuPalace.latitude, Places.compuPalace.longitude)
        markerPrueba = googleMap.addMarker(MarkerOptions().position(prueba).title("Prueba Marker"))*/

/*
     val dragMarker = LatLng(Places.realPlaza.latitude,Places.realPlaza.longitude)
        markerDrag = googleMap.addMarker(MarkerOptions().position(dragMarker).title("CyberPlaza").draggable(true))
*/


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(am, 10f))
        //googleMap.setOnMarkerClickListener(this)
        //googleMap.setOnMarkerDragListener(this)
        //googleMap.setOnInfoWindowClickListener(this)
    }

/*     override fun onMarkerClick(p0: Marker?): Boolean {
         if (p0?.equals(markerPrueba)!!) {
             Toast.makeText(
                 this,
                 "Mi primer evento ${markerPrueba.position.latitude} ${markerPrueba.position.longitude}",
                 Toast.LENGTH_SHORT
             ).show()
         }
         return false

     }*/

/*  override fun onMarkerDragEnd(p0: Marker?) {
        if (p0?.equals(markerDrag)!!) {
            //Toast.makeText(this, "Finalizo arrastre", Toast.LENGTH_SHORT).show()
            //supportActionBar?.title = "Map"
            getAddress(p0.position.latitude,p0.position.longitude)
        }

    }

    override fun onMarkerDragStart(p0: Marker?) {
        if (p0?.equals(markerDrag)!!) {
            Toast.makeText(this, "Inicio arrastre", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onMarkerDrag(p0: Marker?) {
        if (p0?.equals(markerDrag)!!) {
            supportActionBar?.title = getString(R.string.marker_detail_latlng, p0.position.latitude,p0.position.longitude)

        }
    }*/

/*     override fun onInfoWindowClick(p0: Marker?) {
         if (p0?.equals(markerPrueba)!!) {

            CompuPalaceDialogFragment.newInstance("Compupalace",getString(R.string.compuPalaceInfo)).show(supportFragmentManager,"dialog")
         }
     }*/

 /*   private fun getAddress(lat: Double, long: Double) {
        try {
             mGeocoder.getFromLocation(lat, long, 1).apply {
                 if (size>0 && this!=null){
                     var lastAddress = this[0]
                     supportActionBar?.title= lastAddress.getAddressLine(0)
                 }
             }
        } catch (e: Exception) {
            Log.d("Error", "Error en geocoder:"+e.toString());
        }
    }*/
}

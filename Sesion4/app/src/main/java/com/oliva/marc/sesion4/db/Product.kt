package com.oliva.marc.sesion4.db

 class Product(val id : Int? , val name :String?, val description:String?){
     override fun toString(): String {
         return "Product : $id  - $name - $description"
     }
 }
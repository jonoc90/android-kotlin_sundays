package com.oliva.marc.sesion4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oliva.marc.sesion4.db.DBHelper
import com.oliva.marc.sesion4.db.DBRepository
import com.oliva.marc.sesion4.db.Product
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sqlite_button.setOnClickListener {
            goToSqliteActivity()
        }
        room_button.setOnClickListener {
            goToRoomActivity()
        }
    }
    private fun goToSqliteActivity(){
        startActivity(Intent(this,SqliteActivity::class.java))
    }
    private fun goToRoomActivity(){
        startActivity(Intent(this,RoomDbActivity::class.java))

    }

}

package com.oliva.marc.sesion4.room

import androidx.room.*

@Dao
interface ProductDao {
    @Query("SELECT * FROM product")
    fun getAll(): List<Product>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(product: Product)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg product: Product)

    @Delete
    fun delete(product: Product)
}

@Dao
interface EmployeesDao{
    @Query("DELETE from product")
    fun sortById():List<Employees>
}
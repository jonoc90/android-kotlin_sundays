package com.oliva.marc.sesion4

import android.app.Application
import com.oliva.marc.sesion4.room.DBRoomRepository

class InventoryApplication : Application(){
    private lateinit var repository:DBRoomRepository
    override fun onCreate() {
        super.onCreate()
        repository = DBRoomRepository(this)
    }
    fun getInventoryRepository():DBRoomRepository{
        return repository
    }
}
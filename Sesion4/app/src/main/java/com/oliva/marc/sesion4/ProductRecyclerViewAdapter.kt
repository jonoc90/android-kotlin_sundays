package com.oliva.marc.sesion4

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oliva.marc.sesion4.room.Product

class ProductRecyclerViewAdapter( private val products: ArrayList<Product>) :
    RecyclerView.Adapter<ProductRecyclerViewAdapter.RecyclerViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.product_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.setDataProduct(products[position])
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) , View.OnClickListener {
        val nameProduct = itemView.findViewById<TextView>(R.id.name_textview)
        val descriptionProduct = itemView.findViewById<TextView>(R.id.description_textview)
        fun setDataProduct(product: Product) {
            nameProduct.text = product.name
            descriptionProduct.text = product.description
        }

        override fun onClick(v: View?) {
            val context = v?.context
            context?.startActivity(Intent(context,DetailProductActivity::class.java))
        }

    }
}
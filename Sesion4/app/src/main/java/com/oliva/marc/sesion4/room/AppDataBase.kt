package com.oliva.marc.sesion4.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Product::class], version = 1)
abstract class AppDatabase : RoomDatabase(){

    abstract fun userDao(): ProductDao

    companion object{
        private var INSTANCE : AppDatabase?=null
        private const val DBName = "inventory.db"

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java, DBName)
                    .build()
                }
            }
            return INSTANCE
        }
    }
    fun destroyInstance(){
        INSTANCE = null
    }

}
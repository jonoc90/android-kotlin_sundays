package com.oliva.marc.sesion4.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        const val DATABASE_NAME = "productsdb"
        const val DATABASE_VERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val SQL_CREATE_ENTRIES =
            "CREATE TABLE ${DBContract.ProductEntry.TABLE_NAME} (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${DBContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME} TEXT," +
                    "${DBContract.ProductEntry.COLUMN_NAME_PRODUCT_DESCRIPTION} TEXT)"
        db?.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${DBContract.ProductEntry.TABLE_NAME}"
        db?.execSQL(SQL_DELETE_ENTRIES)
    }

}
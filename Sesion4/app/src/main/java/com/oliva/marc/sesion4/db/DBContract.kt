package com.oliva.marc.sesion4.db

import android.provider.BaseColumns

object DBContract{

    object ProductEntry : BaseColumns {
        const val TABLE_NAME = "products"
        const val COLUMN_NAME_PRODUCT_NAME = "name"
        const val COLUMN_NAME_PRODUCT_DESCRIPTION = "description"

    }
    object EmployeesEntry  : BaseColumns {

    }
}

package com.oliva.marc.sesion4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oliva.marc.sesion4.db.DBHelper
import com.oliva.marc.sesion4.db.DBRepository
import com.oliva.marc.sesion4.db.Product
import kotlinx.android.synthetic.main.activity_sqlite.*

class SqliteActivity : AppCompatActivity() {

    private var st: StringBuilder = StringBuilder()
    private lateinit var db: DBRepository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sqlite)
        showProducts()
        ui()
    }
    private fun ui(){
        add_button.setOnClickListener {
            val product = Product(null, "arroz", "blanco")
            db.addProduct(product)
            updateListProducts()
        }
        update_button.setOnClickListener {
            val product = Product(2, "arroz editado", "blanco editado")
            db.updateProduct(product)
            updateListProducts()
        }
        delete_button.setOnClickListener {
            val product = Product(2, "arroz editado", "blanco editado")
            db.deleteProduct(product)
            updateListProducts()
        }
        delete_all_button.setOnClickListener {
            db.deleteAll()
            updateListProducts()
        }
    }

    private fun showProducts() {
        db = DBRepository(DBHelper(this))
        val products = db.getProducts()
        products.forEach {
            st.appendln("id = ${it.id} name = ${it.name} desciption = ${it.description}")
        }
        products_textview.text = st.toString()
    }

    private fun updateListProducts() {
        st.clear()
        val products = db.getProducts()
        products.forEach {
            st.appendln("id = ${it.id} name = ${it.name} desciption = ${it.description}")
        }
        products_textview.text = st.toString()
    }
}

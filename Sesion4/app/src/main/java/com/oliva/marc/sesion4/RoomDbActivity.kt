package com.oliva.marc.sesion4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.oliva.marc.sesion4.room.DBRoomRepository
import com.oliva.marc.sesion4.room.Product
import kotlinx.android.synthetic.main.activity_room_db.*

class RoomDbActivity : AppCompatActivity() {

    private var repository: DBRoomRepository? = null
    private var products: ArrayList<Product>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_db)
        setUpRepository()
        fab.setOnClickListener {
            goToAddProduct()
        }
        list_products_recyclerview.layoutManager = LinearLayoutManager(this)
        list_products_recyclerview.adapter = ProductRecyclerViewAdapter(products!!)
    }

    private fun setUpRepository() {
        repository = (application as InventoryApplication).getInventoryRepository()
    }

    private fun goToAddProduct() {
        startActivity(Intent(this, DetailProductActivity::class.java))
    }

}

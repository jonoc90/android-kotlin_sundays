package com.oliva.marc.sesion4.db

import android.content.ContentValues
import android.provider.BaseColumns

class DBRepository(val db: DBHelper) {


    fun addProduct(product: Product) {
        val sqliteDb = db.writableDatabase

        val contentValues = ContentValues().apply {
            put(DBContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME, product.name)
            put(DBContract.ProductEntry.COLUMN_NAME_PRODUCT_DESCRIPTION, product.description)
        }

        sqliteDb.insert(DBContract.ProductEntry.TABLE_NAME, null,contentValues)

        sqliteDb.close()
    }

    fun getProducts():List<Product>{
        val sqliteDb = db.readableDatabase
        val products = mutableListOf<Product>()
        val query = "SELECT * FROM ${DBContract.ProductEntry.TABLE_NAME}"
        val cursor = sqliteDb.rawQuery(query,null)
        if (cursor.moveToFirst()){
            do {
                val product = Product(cursor.getString(0).toInt(),
                    cursor.getString(1),
                    cursor.getString(2))

                products.add(product)
            }while (cursor.moveToNext())
        }
        sqliteDb.close()
        return products.toList()

    }

    fun deleteProduct(product: Product):Int{
        val sqliteDb = db.writableDatabase

        val selection = "${BaseColumns._ID} = ?"

        val selectionArgs = arrayOf(product.id.toString())

        return sqliteDb.delete(DBContract.ProductEntry.TABLE_NAME, selection, selectionArgs)


    }

    fun updateProduct(product: Product):Int{
        val sqliteDb = db.writableDatabase
        val values = ContentValues().apply {
            put(DBContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME, product.name)
            put(DBContract.ProductEntry.COLUMN_NAME_PRODUCT_DESCRIPTION, product.description)

        }
        val selection = "${BaseColumns._ID} = ?"

        val selectionArgs = arrayOf(product.id.toString())
        val row = sqliteDb.update(DBContract.ProductEntry.TABLE_NAME,
            values,selection,selectionArgs)
        sqliteDb.close()
        return row

    }

    fun deleteAll(){
        val sqlite =db.writableDatabase
        sqlite.delete(DBContract.ProductEntry.TABLE_NAME,null,null)
    }
}
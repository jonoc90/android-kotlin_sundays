package com.oliva.marc.proyectofinal.model.repository

import androidx.lifecycle.MutableLiveData
import com.oliva.marc.proyectofinal.model.entities.Plato

interface RestaurantRepository {
    fun getAllPlatos(): MutableLiveData<List<Plato>>
    fun callPlatos()
}
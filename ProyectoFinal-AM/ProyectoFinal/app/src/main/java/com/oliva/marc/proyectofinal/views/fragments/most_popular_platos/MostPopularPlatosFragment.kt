package com.oliva.marc.proyectofinal.views.fragments.most_popular_platos

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.oliva.marc.proyectofinal.R

class MostPopularPlatosFragment : Fragment() {

    companion object {
        fun newInstance() = MostPopularPlatosFragment()
    }

    private lateinit var viewModel: MostPopularPlatosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.most_popular_platos_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MostPopularPlatosViewModel::class.java)
        // TODO: Use the ViewModel
    }

}

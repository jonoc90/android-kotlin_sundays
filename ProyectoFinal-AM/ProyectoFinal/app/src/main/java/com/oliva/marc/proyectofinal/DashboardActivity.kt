package com.oliva.marc.proyectofinal

import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.internal.NavigationMenuView
import com.oliva.marc.proyectofinal.ui.chef.ChefFragment
import com.oliva.marc.proyectofinal.ui.home.HomeFragment
import com.oliva.marc.proyectofinal.ui.menu.MenuFragment
import com.oliva.marc.proyectofinal.ui.orders.OrdersFragment
import kotlinx.android.synthetic.main.app_bar_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var homeFragment:Fragment
    private lateinit var menuFragment : Fragment
    private lateinit var ordersFragment: Fragment
    private lateinit var chefFragment: Fragment
    private lateinit var activeFragment :Fragment
    private val fm = supportFragmentManager

    private val mOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener{item ->
        when(item.itemId){
            R.id.nav_home ->{
                item.isChecked = true
                fm.beginTransaction().hide(activeFragment).show(homeFragment).commit()
                activeFragment = homeFragment
                drawerLayout.closeDrawer(GravityCompat.START)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_menu->{
                item.isChecked = true
                fm.beginTransaction().hide(activeFragment).show(menuFragment).commit()
                activeFragment = menuFragment
                drawerLayout.closeDrawer(GravityCompat.START)
                return@OnNavigationItemSelectedListener true

            }
            R.id.nav_orders->{
                item.isChecked = true
                fm.beginTransaction().hide(activeFragment).show(ordersFragment).commit()
                activeFragment = ordersFragment
                drawerLayout.closeDrawer(GravityCompat.START)
                return@OnNavigationItemSelectedListener true

            }
            R.id.nav_chef->{
                item.isChecked = true
                fm.beginTransaction().hide(activeFragment).show(chefFragment).commit()
                activeFragment = chefFragment
                drawerLayout.closeDrawer(GravityCompat.START)
                return@OnNavigationItemSelectedListener true
            }
        }
        return@OnNavigationItemSelectedListener false
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        //val navController = findNavController(R.id.nav_host_fragment)

        //Inicializar fragmentos
        homeFragment = HomeFragment()
        menuFragment = MenuFragment()
        ordersFragment = OrdersFragment()
        chefFragment = ChefFragment()

        activeFragment = homeFragment

        fm.beginTransaction().add(R.id.nav_host_fragment,chefFragment,"4").hide(chefFragment).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment,ordersFragment,"3").hide(ordersFragment).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment,menuFragment,"2").hide(menuFragment).commit()
        fm.beginTransaction().add(R.id.nav_host_fragment,homeFragment,"1").commit()

        //Agregar un divider personalizado
        val navMenu = navView.getChildAt(0) as NavigationMenuView
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        getDrawable(R.drawable.line_separator)?.let { divider.setDrawable(it) }
        navMenu.addItemDecoration(divider)
        hamburguer_button.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
 /*       appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )*/
     /*   setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)*/
        navView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }


}

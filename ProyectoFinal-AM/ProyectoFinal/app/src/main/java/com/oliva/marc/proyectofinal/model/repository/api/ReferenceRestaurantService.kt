package com.oliva.marc.proyectofinal.model.repository.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ReferenceRestaurantService {
    companion object{
        private val URL = "https://my-restaurant-backend.herokuapp.com/"
    }
    fun getRestaurantService():RestaurantService{
        var retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(RestaurantService::class.java)
    }
}
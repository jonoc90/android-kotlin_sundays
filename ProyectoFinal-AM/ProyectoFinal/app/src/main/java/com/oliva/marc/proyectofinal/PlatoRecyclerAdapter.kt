package com.oliva.marc.proyectofinal

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.navOptions
import androidx.recyclerview.widget.RecyclerView
import com.oliva.marc.proyectofinal.model.entities.Plato
import com.oliva.marc.proyectofinal.ui.home.HomeFragment
import com.oliva.marc.proyectofinal.views.DetailPlatoActivity
import com.oliva.marc.proyectofinal.views.fragments.default_platos.DefaultPlatosFragment
import com.oliva.marc.proyectofinal.views.fragments.detail_plato.DetailPlatoFragment
import com.squareup.picasso.Picasso

class PlatoRecyclerAdapter() :
    RecyclerView.Adapter<PlatoRecyclerAdapter.ViewHolder>() {
    var platos: List<Plato>?=null

    fun setPlatosList(platosList: List<Plato>) {
        platos = platosList

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        var mFm : FragmentManager?=null
        val namePlato = itemView.findViewById<TextView>(R.id.name_plato_textview)
        val descriptionPlato = itemView.findViewById<TextView>(R.id.description_plato_textview)
        val pricePlato = itemView.findViewById<TextView>(R.id.price_plato_textview)
        val imagePlato = itemView.findViewById<ImageView>(R.id.image_plato_imageview)
        fun setDataPlato(plato: Plato) {
            namePlato.text = plato.nombre
            descriptionPlato.text = plato.descripcion
            pricePlato.text = plato.precio

            //picasso for into image in imageview
        Picasso.get().load(plato.imagen).error(R.drawable.food).into(imagePlato)

        }
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            v?.context?.startActivity(Intent(v.context,DetailPlatoActivity::class.java))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_plato, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return platos?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        platos?.get(position)?.let { holder.setDataPlato(it) }
    }
}
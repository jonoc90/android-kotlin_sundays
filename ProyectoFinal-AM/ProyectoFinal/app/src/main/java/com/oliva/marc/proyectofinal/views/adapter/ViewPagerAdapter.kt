package com.oliva.marc.proyectofinal.views.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.oliva.marc.proyectofinal.views.fragments.FoodOneFragment
import com.oliva.marc.proyectofinal.views.fragments.FoodSecondFragment

class ViewPagerAdapter(fm : FragmentManager, val arrayFragments : ArrayList<Fragment>,var titles :MutableList<String>?) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when(position){
            0->arrayFragments[0]
            1->arrayFragments[1]
            else ->arrayFragments[0]
        }
    }

    override fun getCount(): Int {
        return  2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
             0->titles?.get(0)
            1->titles?.get(1)
            else->titles?.get(0)
        }
    }

}
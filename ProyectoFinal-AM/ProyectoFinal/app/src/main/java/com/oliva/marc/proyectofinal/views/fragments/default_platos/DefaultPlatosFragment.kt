package com.oliva.marc.proyectofinal.views.fragments.default_platos

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.oliva.marc.proyectofinal.R
import com.oliva.marc.proyectofinal.model.entities.Plato
import kotlinx.android.synthetic.main.default_platos_fragment.*

class DefaultPlatosFragment : Fragment() {

    companion object {
        fun newInstance() =
            DefaultPlatosFragment()
    }

    private lateinit var viewModel: DefaultPlatosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.default_platos_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DefaultPlatosViewModel::class.java)
        viewModel.callPlatos()
        viewModel.getPlatos().observe(viewLifecycleOwner, Observer {
                platos:List<Plato> ->
            viewModel.setPlatosInRecyclerAdapter(platos)
        })

        default_platos_recyclerview.adapter = viewModel.getRecyclerPlatosAdapter()

    }

}

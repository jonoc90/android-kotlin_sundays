package com.oliva.marc.proyectofinal.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.oliva.marc.proyectofinal.R
import com.oliva.marc.proyectofinal.model.entities.Plato
import com.oliva.marc.proyectofinal.views.adapter.ViewPagerAdapter
import com.oliva.marc.proyectofinal.views.fragments.FoodOneFragment
import com.oliva.marc.proyectofinal.views.fragments.FoodSecondFragment
import com.oliva.marc.proyectofinal.views.fragments.default_platos.DefaultPlatosFragment
import com.oliva.marc.proyectofinal.views.fragments.most_popular_platos.MostPopularPlatosFragment
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
       val carouselSlideInterval : Long = 3000
        val imageResourceIds = intArrayOf(
            R.drawable.food,
            R.drawable.food_two,
            R.drawable.food_three,
            R.drawable.food,
            R.drawable.food_two
        )
        view?.food_viewpager?.setData(fragmentManager,imageResourceIds,carouselSlideInterval)
        val arrayFoodListFragments = ArrayList<Fragment>()
        arrayFoodListFragments.add(DefaultPlatosFragment())
        arrayFoodListFragments.add(MostPopularPlatosFragment())
        val titles = mutableListOf("Menu","Los mas pedidos")
        view?.menu_viewpager?.adapter =  fragmentManager?.let { ViewPagerAdapter(it,arrayFoodListFragments,titles) }

        view?.menu_tablayout?.setupWithViewPager(view?.menu_viewpager)

        super.onActivityCreated(savedInstanceState)
    }
}

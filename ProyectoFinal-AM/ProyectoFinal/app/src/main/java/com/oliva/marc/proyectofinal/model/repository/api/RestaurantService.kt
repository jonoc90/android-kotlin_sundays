package com.oliva.marc.proyectofinal.model.repository.api

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET

interface RestaurantService {
    @GET("platos")
    fun getPlatos():Call<JsonObject>
}
package com.oliva.marc.proyectofinal.model.entities

import com.google.gson.JsonObject
import java.io.Serializable
import java.lang.Exception

/*"_id": "5de89e2904de089dac64f03c",
"categoria_id": {
    "_id": "5debb5a13655c27f841e66b1",
    "descripcion": "Platos de fondo",
    "nombre": "Segundos"
},
"nombre": "Arroz con pollo",
"precio": 12,
"descripcion": null*/
class Plato(plato: JsonObject?):Serializable {
    lateinit var imagen: String
    lateinit var _id: String
    lateinit var categoria_id: JsonObject
    lateinit var nombre: String
    lateinit var precio: String
    var descripcion: String?=null

    init {
        try {
            _id = plato!!.get("_id").asString
            imagen = plato!!.get("imagen").asString
            categoria_id = plato?.get("categoria_id").asJsonObject
            nombre = plato?.get("nombre").asString
            precio = plato?.get("precio").asString
            descripcion = plato.get("descripcion").asString
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
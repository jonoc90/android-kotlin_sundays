package com.oliva.marc.proyectofinal.views.fragments.default_platos

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.oliva.marc.proyectofinal.PlatoRecyclerAdapter
import com.oliva.marc.proyectofinal.model.RestaurantObservable
import com.oliva.marc.proyectofinal.model.entities.Plato
import com.oliva.marc.proyectofinal.ui.home.HomeFragment

class DefaultPlatosViewModel : ViewModel() {
    private var restaurantObservable: RestaurantObservable = RestaurantObservable()
    private var platoRecyclerAdapter: PlatoRecyclerAdapter? =null
    fun callPlatos(){
        restaurantObservable.callPlatos()
    }
    fun getPlatos(): MutableLiveData<List<Plato>> {
        return restaurantObservable.getPlatos()
    }

    fun setPlatosInRecyclerAdapter (platos:List<Plato>){
        platoRecyclerAdapter?.setPlatosList(platos)
        platoRecyclerAdapter?.notifyDataSetChanged()
    }
    fun getRecyclerPlatosAdapter(): PlatoRecyclerAdapter?{
        platoRecyclerAdapter = PlatoRecyclerAdapter()
        return platoRecyclerAdapter
    }
}

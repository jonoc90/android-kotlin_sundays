package com.oliva.marc.proyectofinal.model.repository.impl

import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.oliva.marc.proyectofinal.model.entities.Plato
import com.oliva.marc.proyectofinal.model.repository.RestaurantRepository
import com.oliva.marc.proyectofinal.model.repository.api.ReferenceRestaurantService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestaurantRepositoryImpl:RestaurantRepository {
    private var platos = MutableLiveData<List<Plato>>()

    override fun getAllPlatos(): MutableLiveData<List<Plato>> {
       return  platos
    }

    override fun callPlatos() {
        var platosList : ArrayList<Plato>? = ArrayList()
        val apiAdapter = ReferenceRestaurantService()
        val apiService = apiAdapter.getRestaurantService()
        val call = apiService.getPlatos()
        call.enqueue(object: Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                t.stackTrace
            }
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val platosJsonObject = response.body()
                val platosArrayObject = platosJsonObject?.getAsJsonArray("data")
                platosArrayObject?.forEach { jsonElement : JsonElement ->
                    val jsonObject= jsonElement.asJsonObject
                    val plato = Plato(jsonObject)
                    platosList?.add(plato)
                }
                platos.value = platosList
            }

        })

    }

}
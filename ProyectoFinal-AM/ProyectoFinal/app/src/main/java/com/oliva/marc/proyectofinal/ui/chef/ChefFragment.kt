package com.oliva.marc.proyectofinal.ui.chef

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.oliva.marc.proyectofinal.R

class ChefFragment : Fragment() {

    companion object {
        fun newInstance() = ChefFragment()
    }

    private lateinit var viewModel: ChefViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.chef_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChefViewModel::class.java)
        // TODO: Use the ViewModel
    }

}

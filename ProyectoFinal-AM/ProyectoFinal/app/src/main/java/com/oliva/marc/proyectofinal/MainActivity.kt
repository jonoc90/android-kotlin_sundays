package com.oliva.marc.proyectofinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ui()
    }
    private fun ui(){
        signup_button.setOnClickListener {
            startActivity(Intent(this,RegisterActivity::class.java))
        }
        signin_button.setOnClickListener {
            startActivity(Intent(this,DashboardActivity::class.java))
        }
    }
}

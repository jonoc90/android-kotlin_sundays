package com.oliva.marc.proyectofinal.model

import androidx.lifecycle.MutableLiveData
import com.oliva.marc.proyectofinal.model.entities.Plato
import com.oliva.marc.proyectofinal.model.repository.RestaurantRepository
import com.oliva.marc.proyectofinal.model.repository.impl.RestaurantRepositoryImpl

class RestaurantObservable {
    private var restaurantRespository :RestaurantRepository = RestaurantRepositoryImpl()
    //repository
    fun callPlatos (){
        restaurantRespository.callPlatos()
    }

    //viewmodel
    fun getPlatos():MutableLiveData<List<Plato>>{
        return restaurantRespository.getAllPlatos()
    }
}
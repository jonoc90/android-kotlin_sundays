package com.oliva.marc.sesion3

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.oliva.marc.sesion3.model.HospitalEntity
import kotlinx.android.synthetic.main.hospital_item.view.*

class HospitalRecyclerViewAdapter(val hospitals: ArrayList<HospitalEntity>) :
    RecyclerView.Adapter<HospitalRecyclerViewAdapter.RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.hospital_item, parent, false)
        )
    }

    override fun getItemCount(): Int = hospitals.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.setDataHospital(hospitals[position])
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        val nameHospital = itemView.name_hospital_textview
        val addressHospital = itemView.address_hospital_textview
        val phoneHospital = itemView.phone_hospital_textview
            init {
                itemView.setOnClickListener(this)
            }
        fun setDataHospital(item: HospitalEntity) {
            nameHospital.text = item.name
            addressHospital.text = item.address
            phoneHospital.text = item.phone
        }
        override fun onClick(p0: View?) {
            val context = p0?.context
          context.let {
             context?.startActivity(Intent(p0.context,FragmentsStaticsActivity::class.java))
          }
        }
    }

}
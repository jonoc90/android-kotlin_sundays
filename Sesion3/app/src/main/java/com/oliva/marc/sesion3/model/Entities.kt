package com.oliva.marc.sesion3.model

data class FriendEntity (
    val name : String,
    val lastName : String
)

data class PetEntity (
    val name : String,
    val weight : Double,
    val height : Double
)
data class HospitalEntity(
    val name : String ,
    val address: String,
    val phone : String
)
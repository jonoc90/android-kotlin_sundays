package com.oliva.marc.sesion3

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.oliva.marc.sesion3.model.PetEntity
import kotlinx.android.synthetic.main.pet_item.view.*

class PetAdapter(private val pets :ArrayList<PetEntity>) : BaseAdapter(){
    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = LayoutInflater.from(p2?.context).inflate(R.layout.pet_item,p2,false)
        view.name_pet_textview.text = pets[p0].name
        view.weight_pet_textview.text = pets[p0].weight.toString()
        view.height_pet_textview.text = pets[p0].height.toString()
        return view
    }

    override fun getItem(p0: Int): Any {
       return pets[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return pets.size
    }

}
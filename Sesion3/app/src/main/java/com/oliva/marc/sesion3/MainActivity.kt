package com.oliva.marc.sesion3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oliva.marc.sesion3.model.FriendEntity
import com.oliva.marc.sesion3.model.PetEntity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val friends = ArrayList<FriendEntity>()
        friends.add(FriendEntity("julio","duglas"))
        friends.add(FriendEntity("julio","duglas"))
        friends.add(FriendEntity("julio","duglas"))
        friends.add(FriendEntity("julio","duglas"))
        val pets = ArrayList<PetEntity>()
        pets.add(PetEntity("julio",1.20,3.2))
        pets.add(PetEntity("julio",1.20,3.2))
        pets.add(PetEntity("julio",1.20,3.2))
        pets.add(PetEntity("julio",1.20,3.2))

        val adapter = FriendsAdapter(friends)
        val adapter2 = PetAdapter(pets)
        friends_listview.adapter = adapter
        pets_listview.adapter = adapter2

        friends_listview.setOnItemClickListener { adapterView, view, i, l ->
            startActivity(Intent(this,DetailListActivity::class.java))
        }
    }
}

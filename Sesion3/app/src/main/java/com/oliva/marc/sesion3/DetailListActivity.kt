package com.oliva.marc.sesion3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.oliva.marc.sesion3.model.HospitalEntity
import kotlinx.android.synthetic.main.activity_detail_list.*

class DetailListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_list)
        val hospitals = ArrayList<HospitalEntity>()
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))
        hospitals.add(HospitalEntity("Rebagliati","Lima","12345-6789"))

        val adapter = HospitalRecyclerViewAdapter(hospitals)
        hospitals_recyclerview.layoutManager = LinearLayoutManager(this)
        hospitals_recyclerview.adapter = adapter
    }
}

package com.oliva.marc.sesion3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_fragments_statics.*

class FragmentsStaticsActivity : AppCompatActivity() {

    lateinit var  active : Fragment
    private val fm = supportFragmentManager
    private val firstFragment = FirstFragment()
    private val secondFragment = SecondFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragments_statics)

        fm.beginTransaction().add(R.id.container,secondFragment).hide(secondFragment).commit()
        fm.beginTransaction().add(R.id.container,firstFragment).commit()
        active = firstFragment

        one_fragment_button.setOnClickListener {
            fm.beginTransaction().show(firstFragment).hide(active).commit()
            active = firstFragment
        }
        second_fragment_button.setOnClickListener {
            fm.beginTransaction().show(secondFragment).hide(active).commit()
            active = secondFragment
        }
    }
}

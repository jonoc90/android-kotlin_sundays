package com.oliva.marc.sesion3


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * A simple [Fragment] subclass.
 */
class FirstFragment : Fragment() {

    override fun onAttach(context: Context) {
        Log.i(FirstFragment::class.java.name,"ON ATTACH")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(FirstFragment::class.java.name,"ON CREATE")
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.i(FirstFragment::class.java.name,"ON CREATEVIEW")

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.i(FirstFragment::class.java.name,"ON ACTIVITYCREATED")
        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        Log.i(FirstFragment::class.java.name,"ON START")

        super.onStart()
    }

    override fun onResume() {
        Log.i(FirstFragment::class.java.name,"ON RESUMEN")

        super.onResume()
    }

    override fun onPause() {
        Log.i(FirstFragment::class.java.name,"ON PAUSE")

        super.onPause()
    }

    override fun onStop() {
        Log.i(FirstFragment::class.java.name,"ON STOP")

        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.i(FirstFragment::class.java.name,"ON DESTROY")

        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }



}

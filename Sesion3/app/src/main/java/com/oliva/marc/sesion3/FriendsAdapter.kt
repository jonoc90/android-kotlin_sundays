package com.oliva.marc.sesion3

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.oliva.marc.sesion3.model.FriendEntity
import kotlinx.android.synthetic.main.friend_item.view.*

class FriendsAdapter(private val friends: ArrayList<FriendEntity>) : BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = LayoutInflater.from(p2?.context).inflate(R.layout.friend_item, p2, false)
        view.name_friend_textview.text = friends[p0].name
        view.lastname_friend_textview.text = friends[p0].lastName
        return view
    }

    override fun getItem(position: Int): Any {
        return friends[position]
    }

    override fun getItemId(p0: Int): Long = p0.toLong()

    override fun getCount(): Int = friends.size

}
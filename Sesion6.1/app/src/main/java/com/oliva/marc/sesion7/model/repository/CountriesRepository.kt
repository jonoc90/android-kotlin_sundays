package com.oliva.marc.sesion7.model.repository

import androidx.lifecycle.MutableLiveData
import com.oliva.marc.sesion7.model.Country

interface CountriesRepository {
    fun getCountries(): MutableLiveData<List<Country>>
    fun callCountriesAPI()
}
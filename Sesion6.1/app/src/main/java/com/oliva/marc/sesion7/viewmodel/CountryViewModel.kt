package com.oliva.marc.sesion7.viewmodel

import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.oliva.marc.sesion7.R
import com.oliva.marc.sesion7.model.Country
import com.oliva.marc.sesion7.model.CountryObservable
import com.oliva.marc.sesion7.view.CountryRecyclerAdapter
import com.oliva.marc.sesion7.view.MainActivity


class CountryViewModel : ViewModel() {
    private var countryObservable: CountryObservable = CountryObservable()
    private var countryRecyclerAdapter: CountryRecyclerAdapter? = null
    fun callCountries() {
        countryObservable.callCountries()
    }

    fun getCountries(): MutableLiveData<List<Country>> {
        return countryObservable.getCountries()
    }

    fun setCountriesInRecyclerAdapter(countries: List<Country>) {
        countryRecyclerAdapter?.setCountriesList(countries)
        countryRecyclerAdapter?.notifyDataSetChanged()
    }
    fun getRecyclerCountriesAdapter(activity: MainActivity): CountryRecyclerAdapter? {
        countryRecyclerAdapter = CountryRecyclerAdapter(activity)
        return countryRecyclerAdapter
    }

}
package com.oliva.marc.sesion7.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ahmadrosid.svgloader.SvgLoader
import com.oliva.marc.sesion7.R
import com.oliva.marc.sesion7.model.Country
import com.oliva.marc.sesion7.viewmodel.CountryViewModel


class CountryRecyclerAdapter( val mActivity: MainActivity) :
    RecyclerView.Adapter<CountryRecyclerAdapter.CardCountryHolder>() {
    var countries: List<Country>?=null
    fun setCountriesList(countries: List<Country>) {
        this.countries = countries
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardCountryHolder {

        return CardCountryHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return countries?.size ?: 0
    }

    override fun onBindViewHolder(holder: CardCountryHolder, position: Int) {

        holder.setDataCard(countries?.get(position), mActivity)
    }

    class CardCountryHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        var country : Country? =null
        val flagImage = itemView.findViewById<ImageView>(R.id.flag_imageview)
        val nameCountry = itemView.findViewById<TextView>(R.id.name_country_textview)

        init {
            itemView.setOnClickListener(this)
        }
        fun setDataCard(country: Country?, activity: MainActivity) {
            this.country = country
            SvgLoader.pluck()
                .with(activity)
                .setPlaceHolder(R.drawable.loading_spinner, R.drawable.loading_spinner)
                .load(country?.uriFlag, flagImage)
            nameCountry.text = country?.name

        }

        override fun onClick(v: View?) {
            val bundle = Bundle()
            bundle.putSerializable("country",country)
            val intent = Intent(v?.context,DetailCountryActivity::class.java)
            intent.putExtra("bundle",bundle)
            v?.context?.startActivity(intent)
        }

    }


}
package com.oliva.marc.sesion7.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.ahmadrosid.svgloader.SvgLoader
import com.oliva.marc.sesion7.R
import com.oliva.marc.sesion7.model.Country
import kotlinx.android.synthetic.main.activity_detail_country.*

class DetailCountryActivity : AppCompatActivity() {

    lateinit var country: Country
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_country)

        ui()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        (item.itemId == android.R.id.home ).apply {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun ui(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Country Detail"
        country = intent.extras?.getBundle("bundle").let {
            it?.getSerializable("country") as Country
        }
        name_country_textview.text = country.name
        SvgLoader.pluck()
            .with(this)
            .setPlaceHolder(R.drawable.loading_spinner,R.drawable.loading_spinner)
            .load(country.uriFlag, flag_imageview)
        capital_textview.text = country.capital
        population_textview.text= country.population
        region_textview.text=country.region
        code_textview.text= country.alpha3Code
        area_textview.text= country.area
    }
}

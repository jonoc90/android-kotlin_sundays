package com.oliva.marc.sesion7.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.oliva.marc.sesion7.R
import com.oliva.marc.sesion7.model.Country
import com.oliva.marc.sesion7.viewmodel.CountryViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var countryViewModel: CountryViewModel?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        Log.i(MainActivity::class.java.name,"ONCREATE")
        setUpViewModel()
        setUpRecycler()
    }
    private fun setUpViewModel(){
        countryViewModel = ViewModelProviders.of(this).get(CountryViewModel::class.java)
        countryViewModel?.callCountries()
        countryViewModel?.getCountries()?.observe(this, Observer {
            countries:List<Country>->
            countryViewModel?.setCountriesInRecyclerAdapter(countries)
        })
    }
    private fun setUpRecycler(){
        countries_recyclerview.adapter = countryViewModel?.getRecyclerCountriesAdapter(this)
    }
}

package com.oliva.marc.sesion7.model


import androidx.lifecycle.MutableLiveData
import com.oliva.marc.sesion7.model.repository.CountriesRepository
import com.oliva.marc.sesion7.model.repository.impl.CountriesRepositoryImpl

class CountryObservable {
    private var countryRepository: CountriesRepository = CountriesRepositoryImpl()
    //Repository
    fun callCountries(){
        countryRepository.callCountriesAPI()
    }
    //ViewModel
    fun getCountries(): MutableLiveData<List<Country>>{
        return countryRepository.getCountries()
    }
}
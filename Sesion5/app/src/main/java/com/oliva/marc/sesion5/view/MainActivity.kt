package com.oliva.marc.sesion5.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oliva.marc.sesion5.R
import com.oliva.marc.sesion5.model.Movie
import com.oliva.marc.sesion5.presenter.MoviePresenter
import com.oliva.marc.sesion5.presenter.MoviePresenterImpl
import java.lang.Exception

class MainActivity : AppCompatActivity(), MovieView {

    private var moviePresenter: MoviePresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpInstances()
        ui()
    }
    fun ui(){
        //getMovies()
        getMoviesFirestore()
    }
    fun setUpInstances(){
        moviePresenter = MoviePresenterImpl(this)
    }
    override fun showMovies(movies: ArrayList<Movie>?) {
        try {

        }catch (e:Exception){

        }
    }

    override fun getMovies() {
        moviePresenter?.getMovies()
    }

    override fun getMoviesFirestore() {
        moviePresenter?.getMoviesFirestore()
    }
}

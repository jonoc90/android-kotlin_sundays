package com.oliva.marc.sesion5.model.repository

interface MoviesRepository {
    fun getMoviesAPI()
    fun getMoviesFirestore()
}
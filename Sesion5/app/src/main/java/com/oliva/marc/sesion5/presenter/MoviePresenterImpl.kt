package com.oliva.marc.sesion5.presenter

import com.oliva.marc.sesion5.model.Movie
import com.oliva.marc.sesion5.model.interactor.MoviesInteractor
import com.oliva.marc.sesion5.model.interactor.impl.MoviesInteractorImpl
import com.oliva.marc.sesion5.view.MovieView

class MoviePresenterImpl(val moviesView: MovieView) :MoviePresenter{
    private val movieInteractor :MoviesInteractor= MoviesInteractorImpl(this)
    override fun showMovies(movies: ArrayList<Movie>?) {
        moviesView.showMovies(movies)
    }

    override fun getMovies() {
        movieInteractor.getMoviesAPI()
    }

    override fun getMoviesFirestore() {
        movieInteractor.getMoviesFirestore()
    }


}
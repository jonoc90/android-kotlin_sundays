package com.oliva.marc.sesion5.model.interactor.impl

import com.oliva.marc.sesion5.model.interactor.MoviesInteractor
import com.oliva.marc.sesion5.model.repository.impl.MoviesRepositoryImpl
import com.oliva.marc.sesion5.presenter.MoviePresenter

class MoviesInteractorImpl(moviePresenter:MoviePresenter) :MoviesInteractor{
    private var moviesRespository = MoviesRepositoryImpl(moviePresenter)
    override fun getMoviesAPI() {
        moviesRespository.getMoviesAPI()
    }

    override fun getMoviesFirestore() {
        moviesRespository.getMoviesFirestore()
    }

}
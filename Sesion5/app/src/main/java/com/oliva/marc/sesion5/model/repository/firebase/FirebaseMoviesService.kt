package com.oliva.marc.sesion5.model.repository.firebase

import com.google.firebase.firestore.*

class FirebaseMoviesService : FirebaseMovie {
    companion object {
        private const val MOVIES_COLLECTION = "movies"
        private const val MOVIES_FIELD_NAME = "name"
        private const val MOVIES_FIELD_PICTURE = "picture"
        private const val MOVIES_FIELD_RATING = "rating"
        private const val MOVIES_FIELD_CATEGORY = "category"
    }

    private val remoteDB = FirebaseFirestore.getInstance().apply {
        firestoreSettings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(false)
            .build()
    }


    override fun getAllMovies(): List<MovieFB> {
        var movies = ArrayList<MovieFB>()
        remoteDB.collection(MOVIES_COLLECTION)
            .get()
            .addOnSuccessListener { it ->
                for (e in it.documents) {
                    val result = e.toObject(MovieFB::class.java)
                    movies.add(result!!)
                }
            }
            .addOnFailureListener {

            }
        return movies

    }

    override fun addMovie(movie: MovieFB) {

        val movieData = HashMap<String, Any>()
        movieData[MOVIES_FIELD_NAME] = movie.name
        movieData[MOVIES_FIELD_PICTURE] = movie.picture
        movieData[MOVIES_FIELD_RATING] = movie.rating
        movieData[MOVIES_FIELD_CATEGORY]= movie.category


        remoteDB.collection(MOVIES_COLLECTION)
            .add(movieData)
            .addOnSuccessListener {

            }
            .addOnFailureListener {

            }

    }


    override fun deleteMovie(movieId: String){

            remoteDB.collection(MOVIES_COLLECTION)
                .document(movieId)
                .delete()
                .addOnSuccessListener {

                }
                .addOnFailureListener {

                }

    }

}
package com.oliva.marc.sesion5.model.repository.firebase

data class MovieFB(
    var name:String="",
    var picture:String="",
    var rating:String="",
    var category:String=""
)
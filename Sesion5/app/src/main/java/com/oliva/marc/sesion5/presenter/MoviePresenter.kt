package com.oliva.marc.sesion5.presenter

import com.oliva.marc.sesion5.model.Movie

interface MoviePresenter {
    fun showMovies(movies:ArrayList<Movie>?)
    fun getMovies()
    fun getMoviesFirestore()
}
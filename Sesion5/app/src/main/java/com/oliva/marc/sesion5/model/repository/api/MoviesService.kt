package com.oliva.marc.sesion5.model.repository.api


import com.google.gson.JsonArray
import retrofit2.Call
import retrofit2.http.GET

interface MoviesService {
    @GET("movie")
    fun listMovies():Call<JsonArray>
}
package com.oliva.marc.sesion5.model.interactor

interface MoviesInteractor {
    fun getMoviesAPI()
    fun getMoviesFirestore()
}
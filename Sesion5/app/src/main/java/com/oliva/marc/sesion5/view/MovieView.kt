package com.oliva.marc.sesion5.view

import com.oliva.marc.sesion5.model.Movie

interface MovieView {
    fun showMovies(movies:ArrayList<Movie>?)
    fun getMovies()
    fun getMoviesFirestore()
}
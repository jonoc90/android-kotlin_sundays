package com.oliva.marc.sesion5.model.repository.impl

import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.oliva.marc.sesion5.model.Movie
import com.oliva.marc.sesion5.model.repository.MoviesRepository
import com.oliva.marc.sesion5.model.repository.api.ReferenceMoviesService
import com.oliva.marc.sesion5.model.repository.firebase.FirebaseMoviesService
import com.oliva.marc.sesion5.presenter.MoviePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesRepositoryImpl(val moviePresenter: MoviePresenter) : MoviesRepository {

    override fun getMoviesAPI() {
        var movies: ArrayList<Movie>? = ArrayList()
        val apiAdapter = ReferenceMoviesService()
        val apiService = apiAdapter.getClientService()
        val call = apiService.listMovies()

        call.enqueue(object : Callback<JsonArray> {
            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                t.stackTrace
            }

            override fun onResponse(call: Call<JsonArray>, response: Response<JsonArray>) {
                val countriesJsonArray = response.body()
                countriesJsonArray?.forEach { jsonElement: JsonElement ->
                    val jsonObject = jsonElement.asJsonObject
                    val country = Movie(jsonObject)
                    movies?.add(country)

                }
                moviePresenter.showMovies(movies)

            }

        })
    }

    override fun getMoviesFirestore() {
        val movies = FirebaseMoviesService().getAllMovies()
        val nose = ""
        //moviePresenter.showMovies(movies)
    }

}